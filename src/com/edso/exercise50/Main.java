package com.edso.exercise50;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private Map<Integer, Location> locations = new HashMap<Integer, Location>();
    private Map<String, String> vocabularies = new HashMap<>();

    public Main() {
        ///memorable definition
        Location quitLocation = new Location(Config.QUIT_LOCATION, "You are sitting in front of a computer learning Java");
        Location endOfRoadLocation = new Location(Config.END_OF_ROAD_LOCATION, "You are standing at the end of a road before a small brick building");
        Location topOfHillLocation = new Location(Config.TOP_OF_HILL_LOCATION, "You are at the top of a hill");
        Location buildingLocation = new Location(Config.BUILDING_LOCATION, "You are inside a building, a well house for a small spring");
        Location valleyLocation = new Location(Config.VALLEY_LOCATION, "You are in a valley beside a stream");
        Location forestLocation = new Location(Config.FOREST_LOCATION, "You are in the forest");

        locations.put(Config.QUIT_LOCATION, quitLocation);
        locations.put(Config.END_OF_ROAD_LOCATION, endOfRoadLocation);
        locations.put(Config.TOP_OF_HILL_LOCATION, topOfHillLocation);
        locations.put(Config.BUILDING_LOCATION, buildingLocation);
        locations.put(Config.VALLEY_LOCATION, valleyLocation);
        locations.put(Config.FOREST_LOCATION, forestLocation);


        //at endOfRoad, we can go west, east, south or north.
        locations.get(Config.END_OF_ROAD_LOCATION).addExit("W", Config.TOP_OF_HILL_LOCATION);
        locations.get(Config.END_OF_ROAD_LOCATION).addExit("E", Config.BUILDING_LOCATION);
        locations.get(Config.END_OF_ROAD_LOCATION).addExit("S", Config.VALLEY_LOCATION);
        locations.get(Config.END_OF_ROAD_LOCATION).addExit("N", Config.FOREST_LOCATION);

        //at topOfHill, we can only go north
        locations.get(Config.TOP_OF_HILL_LOCATION).addExit("N", Config.FOREST_LOCATION);

        //at building, we can only go west
        locations.get(Config.BUILDING_LOCATION).addExit("W", Config.END_OF_ROAD_LOCATION);

        //at valley, we can go north or west
        locations.get(Config.VALLEY_LOCATION).addExit("N", Config.END_OF_ROAD_LOCATION);
        locations.get(Config.VALLEY_LOCATION).addExit("W", Config.TOP_OF_HILL_LOCATION);

        //at forest, we can go south or west
        locations.get(Config.FOREST_LOCATION).addExit("S", Config.END_OF_ROAD_LOCATION);
        locations.get(Config.FOREST_LOCATION).addExit("W", Config.TOP_OF_HILL_LOCATION);



        ///add new vocabulary
        vocabularies.put("NORTH", "N");
        vocabularies.put("SOUTH", "S");
        vocabularies.put("EAST", "E");
        vocabularies.put("WEST", "W");
        vocabularies.put("QUIT", "Q");


    }

    public void command() {
        Scanner scanner = new Scanner(System.in);
        // write code here
        int locationId = Config.END_OF_ROAD_LOCATION;  //start at endOfRoadLocation
        while (true){
            //print firstLocation and exits
            Location firstLocation = this.locations.get(locationId);
            System.out.println(firstLocation.getDescription());
            if(locationId == Config.QUIT_LOCATION){  // if quit
                break;            // end while
            }
            Map<String, Integer> exits = firstLocation.getExits();
            System.out.print("Available exits are: ");
            for(String i : exits.keySet()){
                System.out.print(i + ",");
            }
            System.out.println("");

            //input here
            String direction = scanner.nextLine().toUpperCase().replace(".", "");
            if(direction.length() > 1){  // change long words to just N, W, E, S, Q
                String[] str = direction.split(" ");
                for(String i : str){
                    if(this.vocabularies.keySet().contains(i)){
                        direction = this.vocabularies.get(i);
                        break;
                    }
                }
            }


            //check if can go this way
            if(exits.containsKey(direction)){
                //move to new location
                locationId = exits.get(direction);
            } else {
                System.out.println("You can't go in that direction");
            }
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.command();
    }
}