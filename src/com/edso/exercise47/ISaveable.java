package com.edso.exercise47;

import java.util.List;

public interface ISaveable {
    public abstract List<String> write();

    public abstract void read(List<String> list);
}
