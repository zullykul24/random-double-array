package com.edso.exercise46;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    // write code here
    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String name, String artist){
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<Song>();
    }

    public boolean addSong(String title, double duration){
        if(this.findSong(title) == null){
            this.songs.add(new Song(title, duration));
            return true;
        }
        return false;
    }

    private Song findSong(String title){
        for(Song i : this.songs){
            if(i.getTitle().equals(title)){
                return i;
            }
        }
        return null;
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> linkedListSongs){
        if(trackNumber < 1 || trackNumber > this.songs.size()){
            //size:9 -> index from 0 to 8
            //track from 1 to 9
            return false;
        }
        linkedListSongs.add(this.songs.get(trackNumber - 1));
        return true;
    }

    public boolean addToPlayList(String title, LinkedList<Song> linkedListSongs){
        Song song = this.findSong(title);
        if(song != null){
            linkedListSongs.add(song);
            return true;
        }
        return false;
    }


}