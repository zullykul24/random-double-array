package com.edso.bai2tuan3_regex;

import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args) {
        String phone = "+84378874128";
        String email = "khoinp.45@gmail.com";

        System.out.println("Is phone valid? " + isVietNamePhoneNumber(phone));
        System.out.println("Is email valid? " + isEmail(email));
    }

    public static boolean isVietNamePhoneNumber(String number){
        Pattern pattern = Pattern.compile("^((\\+?84)|0)(\\d{9})$");
        return pattern.matcher(number).matches();
    }

    public static boolean isEmail(String email){
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$");
        return pattern.matcher(email).matches();
    }
}
