package com.edso.baitaptuan3;

import java.util.TimerTask;

public class QueueSizeCheckTask extends TimerTask {
    @Override
    public void run() {
        try {
            System.out.println(ThreadColor.ANSI_RESET + "-----Queue size = " + Main.messenger.size());
        } catch (NullPointerException e){
            System.out.println(ThreadColor.ANSI_RESET + "Queue is null");
        }

    }
}
