package com.edso.bai2tuan2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MultiThreadSorting {
    //create newList to store sorted lists
    private static List<Integer> newList = new ArrayList<>();

    public static void performSort(final List<Integer> list, final int divisor, final String outFilePath) {
        //the last part could be smaller
        int maxLengthOfPart = (list.size() % divisor == 0) ? list.size()/divisor : list.size()/divisor + 1;

        //split the list to 'divisor' sublists
        List<List<Integer>> parts = chopped(list, maxLengthOfPart);

        List<SortThread> threads = new ArrayList<SortThread>();
        for(int i = 0; i < divisor; i++){
            threads.add(new SortThread(parts.get(i), ThreadColor.ANSI_GREEN));
            threads.get(i).start();
        }

        new Thread(){
            @Override
            public void run() {
                for(int i = 0; i < divisor; i++){
                    try {
                        threads.get(i).join(); //to wait for all the sublists to be sorted
                    } catch (InterruptedException e){

                    }
                }
                for(int i = 0; i < divisor; i++){
                    newList.addAll(parts.get(i));
                }

                //then sort 1 more time
                Collections.sort(newList);
                //System.out.println(newList);


                ///Write file here
                try(BufferedWriter file = new BufferedWriter(new FileWriter(outFilePath))) {
                    for(int i : newList){
                        file.write(i + " ");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    //chop the big list into smaller lists
    public static <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
}