package com.edso.bai2tuan2;

import java.util.Collections;
import java.util.List;

public class SortThread extends Thread {
    private List<Integer> list;
    private String color;

    public SortThread(List<Integer> list, String color){
        this.list = list;
        this.color = color;
    }


    @Override
    public synchronized void run() {
        Collections.sort(list);
        // System.out.println(color + "List sorted: " + list);

    }
}
