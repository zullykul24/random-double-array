package com.edso.bai4tuan3_client_server.client;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class SendMessageThread implements Runnable{
    private final Socket socket;

    public SendMessageThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        PrintWriter stringToEcho;
        try {
            stringToEcho = new PrintWriter(socket.getOutputStream(), true);
            Scanner scanner = new Scanner(System.in);
            while (true) {
                String echoString = scanner.nextLine();
                if (echoString.equals("exit")) {
                    stringToEcho.println("exit");
                    socket.close();
                }
                stringToEcho.println(echoString);
            }
        } catch (Exception e) {
            System.out.println("You are disconnected to the server");
            e.printStackTrace();
        }

    }
}
