package com.edso.bai4tuan3_client_server.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.LinkedHashSet;
import java.util.Set;

public class MessagingServer {
    public static Set<PrintWriter> writers = new LinkedHashSet<>();

    public static void main(String[] args) {
        try(ServerSocket serverSocket = new ServerSocket(5000)){
            while (true){
                new Echoer(serverSocket.accept()).start();
            }
        } catch (IOException e){
            System.out.println("Server exception: " + e);
        }
    }
}
