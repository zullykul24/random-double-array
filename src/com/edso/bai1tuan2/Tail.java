package com.edso.bai1tuan2;

import java.io.*;

public class Tail {

    //first read the file to count number of lines of the file
    //then we read the file again but print out from the numLine th line of the file
    public static void readLastLines(int numLine, String filePath) {
        int count = 0;
        try(BufferedReader file = new BufferedReader(new FileReader(filePath));
            BufferedReader reread = new BufferedReader(new FileReader(filePath))){
            String input;
            //count number of lines
            while (file.readLine() != null){
                count++;
            }

            file.close();
           // System.out.println(count);

            //jump to (count - numLine) line to read last numLime lines
            if(count < numLine){
                System.out.println("File has only " + count + " lines");
                return;
            } else {
                for(int i = 0; i < count - numLine; i++){
                    reread.readLine();
                }
            }
            //now read last numLine lines
            while ((input = reread.readLine()) != null){
                System.out.println(input);
            }
        } catch (FileNotFoundException e){
            System.out.println("File không tồn tại");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
