package com.edso.exercise49;

public class MyLinkedList implements NodeList {
    ListItem root = null;

    public MyLinkedList(ListItem root){
        this.root = root;
    }

    @Override
    public ListItem getRoot() {
        return this.root;
    }

    @Override
    public boolean addItem(ListItem item) {
        if(this.root == null){
            this.root = item;
            return true;
        }
        ListItem currentItem = this.root;

        while (currentItem != null){
            int comparison = currentItem.compareTo(item);
            if(comparison < 0){
                //currentItem is smaller
                //move right
                if(currentItem.next() != null){
                    currentItem = currentItem.next();
                } else {
                    currentItem.setNext(item);
                    item.setPrevious(currentItem);
                    return true;
                }
            }
            else if(comparison > 0){
                //currentItem is greater
                //move left
                if(currentItem.previous() != null){
                    currentItem.previous().setNext(item);
                    item.setPrevious(currentItem.previous());
                    item.setNext(currentItem);
                    currentItem.setPrevious(item);

                } else {
                    item.setNext(this.root);
                    this.root.setPrevious(item);
                    this.root = item;
                }
                return true;
            } else {
                // i equals item
                return false;
            }
        }

        return false;
    }

    @Override
    public boolean removeItem(ListItem item) {
        if(this.root == null || item == null){
            return false;
        }
        ListItem currentItem = this.root;


        while(currentItem != null){
            int comparison = currentItem.compareTo(item);
            if(comparison > 0){
                currentItem = currentItem.previous();
            } else if(comparison < 0){
                currentItem = currentItem.next();
            } else {
                //equal
                if(currentItem == this.root){
                    this.root = currentItem.next();
                } else {
                    currentItem.previous().setNext(currentItem.next());
                    if(currentItem.next() != null){
                        currentItem.next().setPrevious(currentItem.previous());
                    }
                }
                return true;
            }

        }

        return false;
    }

    @Override
    public void traverse(ListItem root) {
        if(root == null){
            System.out.println("The list is empty");
        }
        else {
            while (root != null){
                System.out.println(root.getValue());
                root = root.next();
            }
        }
    }

    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList(new Node("100"));
        linkedList.addItem(new Node("43"));
        linkedList.addItem(new Node("50"));
        linkedList.addItem(new Node("150"));
        linkedList.traverse(linkedList.getRoot());
        System.out.println("Delete the item has value 50");
        linkedList.removeItem(new Node("50"));
        linkedList.traverse(linkedList.getRoot());
        System.out.println("Let's delete the root");
        linkedList.removeItem(new Node("100"));
        linkedList.traverse(linkedList.getRoot());
    }
}
